var express = require('express');
var app = express();

app.use(express.static(__dirname));

app.get('/', function(req, res) {
	console.log('hit');
	res.render('index', function(err, html) {
		if (err) {
			console.log(err);
		}
		else {
			res.send(html)
		}
	});
});

app.listen(8081);