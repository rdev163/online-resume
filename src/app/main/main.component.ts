import {Component, OnInit, ViewContainerRef} from '@angular/core';
import {ToastsManager} from 'ng2-toastr';
import '../styles/main.scss';

@Component({
  	selector: 'main',
	template: `
	<div class="container">
		<router-outlet></router-outlet>
	 	<div class="navbar navbar-default navbar-fixed-bottom" role="navigation">
			<ul class="nav nav-tabs nav-justified">
				 <li>
					 <a role="link" class="tab" [routerLink]="[RESUME]">Résumé</a>
				 </li>
				 <li>
					 <a role="link" class="tab" [routerLink]="[ABOUT]">About</a>
				 </li>
			</ul>
		</div>
	</div>
`
})
export class MainComponent implements OnInit {
	public get ABOUT(): string {
		return 'about';
	}
	public get RESUME(): string {
		return 'resume';
	}
	constructor(private toastr: ToastsManager,
	public vRef: ViewContainerRef) {
		this.toastr.setRootViewContainerRef(vRef);
	}
	public ngOnInit(): void {
		this.toastr.info('Welcome to my online résumé, touch to dismiss.', '', {dismiss: 'click'});
}
}
