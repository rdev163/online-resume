import {Component, OnInit} from '@angular/core';
import {Contacts, Education, Experience, Resume, TechBox} from '../models/resume.model';
import {SocketService} from '../services/socket.service';

@Component({
  	selector: 'resume-component',
  	template: `
	<div class="pull-right">
		<strong>Status: </strong><span class="alert-success">Available</span>
		<br>
		<strong>Updated: </strong>{{resume.updated_at | date: "MM/dd/yyyy"}}
	</div>
	<hr>
	<h2>{{resume.title}}</h2>
	<a href="tel:+1{{contacts.phone}}">Phone: {{contacts.phone}}</a>
	<br>
	<a  href="mailto:{{contacts.email}}">Email: {{contacts.email}}</a>
	<br>
	<a href="skype:live:rdev1163?call">Skype: live:rdev1163</a>
	<br>
	<a href="//drive.google.com/open?id=0B3xj0VR9rhCjR19MMFBVeUpSQ0k"><i class="glyphicon glyphicon-open-file"></i> Download Résumé</a>
	<hr>
	<div class="row main">
		<p>{{resume.introduction}}</p>
		<p><b class="large-text text-left">Professional Experience</b></p>
		<div class="row">
			<header class="col-xs-4">Business Name</header>
			<content class="col-xs-7">{{experience.businessName}}</content>
		</div>
		<div class="row">
			<header class="col-xs-4">Length of Work</header>
			<content class="col-xs-7 text-left">{{experience.timeLength}}</content>
		</div>
		<div class="row">
			<header class="col-xs-4">Position</header>
			<content class="col-xs-7">{{experience.position}}</content>
		</div>
		<div class="row">
			<header class="col-xs-12 text-left">Description of Responsibilities</header>
			<content class="col-xs-12">{{experience.briefDuties}}</content>
		</div>
		<p><b class="large-text">Technical Toolbox</b></p>
		<div *ngFor="let item of techBoxContent">
			<div class="row">
				<header class="col-xs-4">{{item.label}}</header>
				<content class="col-xs-7">{{item.description}}</content>
			</div>
		</div>    
		<p><b class="large-text">Education</b></p>
		<div class="row">
			<header class="col-xs-4 text-left">Type:</header>
			<content class="col-xs-7">{{education.schoolName}}</content>
		</div>
		<div class="row">
			<div *ngFor="let segment of educationStudies">
				<header class="col-xs-4 text-nowrap">{{segment.label}}</header>
				<div class="col-xs-8 text-left">
					<content>{{segment.description}}</content>
				</div>
			</div>
		</div>
		<div class="row">
			<header class="col-xs-12">Description of Focus</header>
			<content class="col-xs-12">{{education.briefFocus}}</content>
		</div>
		<messenger-component></messenger-component>
	</div>
`
})

export class ResumeComponent implements OnInit {
	public resume: Resume = <Resume>{};
	public contacts: Contacts = <Contacts>{};
	public techBox: TechBox = <TechBox>{};
	public education: Education = <Education>{};
	public experience: Experience = <Experience>{};
	public techBoxContent: {}[] = [];
	public educationStudies: {}[] = [];

	constructor(private socketService: SocketService){}

	public ngOnInit() {
		this.socketService.socketCouple('resume.get', {}).then((response: any) => {
			this.contacts = response.contact;
			this.resume = {
				title: response.title,
				introduction: response.introduction,
				closure: response.closure,
				updated_at: response.updated_at
			};
			this.education = response.education;
			this.experience = response.experience;
			this.techBox = response.techBox;
			let temp = JSON.parse(response.techBox.content);
			for (let key of Object.keys(temp)) {
				this.techBoxContent.push({label: key, description: temp[key]});
			}
			temp = JSON.parse(response.education.studies);
			for (let key of Object.keys(temp)) {
				this.educationStudies.push({label: key, description: temp[key]});
			}
		});
	}
}