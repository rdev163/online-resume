import {Component} from '@angular/core';

@Component({
	selector: 'about-component',
	template:`
	<div class="row main">
		<div class="row">
			<h4>About This Résumé</h4>
			<p>This résumé is intended to be a demonstration of my abilities to make functional mobile friendly web applications. The source code is available at in my GitLab repos <a href="//gitlab.com/rdev163/online-resume">Rob's Online Resume</a> for the frontend code and the <a href="//gitlab.com/rdev163/online-resume">Online Résumé Backend</a> for my Mailgun API and a Sequelize ORM.</p>
		</div>
		<div class="row">
			<h4>Who am I</h4>
			<p>I am a jack of many trades, especially in web development. I know a bit of server side and a little bit more on client side, a bit of design and a few bits of graphics artistry. I consider myself a <i>Frontend Developer</i>. I make a point to be able to work with anyone and enthusiastic about solving problems. I write music to slip away from problematic perceptions and I read fantasy novels to give contrast to this planet I inhabit. I am a dreamer and an engineer. An optimist at heart and dedicated coder. I am in a state of constant learning and I am just me. My name is Rob, Hello World.</p>
		</div>
		<div class="row">
			<h4>How I Made This</h4>
			<p>This application is comprised of two basic modular components, an <a href="//angular.io">Angular 2 (4.*.*)</a> frontend and an <a href="//expressjs.com" >ExpressJS</a> backend in <a href="//nodejs.org">NodeJS</a>. I am hosting <b>Rob's Online Resume</b> from <a href="http://aws.amazon.com/">Amazon Web Services</a> and using a Linux based virtual machine.</p>
		</div>
		<messenger-component></messenger-component>
	</div>
	`
})

export class AboutComponent {
}