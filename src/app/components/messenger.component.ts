import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {SocketService} from '../services/socket.service';
import {ToastsManager} from 'ng2-toastr';

@Component({
	selector: 'messenger-component',
	template: `
	<h6><header class="text-center">Send Me a Message!</header></h6>
	<div class="row">
		<div class="input-field col-xs-12">
			<input type="text" id="your_name" [formControl]="nameControl" [(ngModel)]="message.sender">
			<label for="your_name">Your Name</label>
		</div>
	</div>
	<div class="row">
		<div class="input-field col-xs-12">
			<input id="email" type="text" class="validate" [(ngModel)]="message.email" [formControl]="emailControl">
			<label for="email">Email</label>
		</div>
	</div>
	<div class="row">
		<div class="input-field col-xs-12">
			<textarea id="icon_prefix2" class="materialize-textarea" [(ngModel)]="message.text" [formControl]="messageControl"></textarea>
			<label for="icon_prefix2">Message</label>
		</div>
	</div>
	<button (click)="send(message)" role="button" type="button" class="btn btn-lg" [disabled]="messengerGroup.invalid" [class.disabled]="messengerGroup.invalid">Submit</button>
`
})

export class MessengerComponent implements OnInit {
	public message: FormMessage = <FormMessage>{};
	public nameControl: FormControl  = new FormControl('', [Validators.required, Validators.maxLength(255)]);
	public messageControl: FormControl = new FormControl('', [Validators.required, Validators.maxLength(255)]);
	public emailControl: FormControl  = new FormControl('', [Validators.required, Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]);
	public messengerGroup: FormGroup = new FormGroup({
		nameControl: this.nameControl,
		messageControl: this.messageControl,
		emailControl: this.emailControl
	});
	constructor(private socketService: SocketService,
				public toastr: ToastsManager){}

	public send(message) {
		this.message = <FormMessage>{};
		this.message.text = 'Thank You!! ;)';
		this.socketService.socketCouple('message', message).then(
			() => {
				this.toastr.success('Message Sent!! :)');
				console.log('returned');
			}, () => {
				this.toastr.error('This service is down at the moment, please try again later, thank you!');
				console.log('reject');
		});
	}

	public ngOnInit(): void {}
}

export interface FormMessage {
	sender: string;
	email: string;
	text: string;
}