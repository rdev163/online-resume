import {Injectable} from '@angular/core';
import * as IO from 'socket.io-client';
import {ToastsManager} from 'ng2-toastr';

@Injectable()
export class SocketService {
	constructor(public toastr: ToastsManager){}

	public socketCouple(path: string, body:any): Promise<any> {
		return new Promise( (resolve) => {
			let socket = IO('ec2-54-202-160-61.us-west-2.compute.amazonaws.com:1729');
			// let socket = IO('localhost:1729');
			socket.on('connect', () => {
			});
			socket.emit(path, body);
			socket.on(path + '.response', response => {
				resolve(response);
				//todo Implement a responseWrapper
				// if (response.status === 'Error') {
				// 	console.log(response.status);
				// } else {
				// 	resolve('success' + response);
				// }
			})
		});
	}
}