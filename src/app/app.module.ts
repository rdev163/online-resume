import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core'
import {RouterModule, Routes} from '@angular/router';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule, NoopAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MainComponent} from './main/main.component';
import {ResumeComponent} from './components/resume.component';
import {HttpModule} from '@angular/http';
import {AboutComponent} from './components/about.component';
import {ToastModule} from 'ng2-toastr';
import {MessengerComponent} from './components/messenger.component';
import {SocketService} from './services/socket.service';

const MAIN_ROUTES: Routes = [
    	{path: 'resume', component: ResumeComponent},
    	{path: 'about', component: AboutComponent},
		{path:'', redirectTo: 'resume', pathMatch: 'full'}
	];

@NgModule({
	declarations: [
    	MainComponent,
		ResumeComponent,
		AboutComponent,
		MessengerComponent
	],
  	imports: [
  		BrowserAnimationsModule,
		NoopAnimationsModule,
  		ToastModule.forRoot(),
		BrowserModule,
		FormsModule,
		ReactiveFormsModule,
		HttpModule,
		RouterModule.forRoot(MAIN_ROUTES)
	],
  	providers: [
		SocketService
  	],
  	schemas: [
    	CUSTOM_ELEMENTS_SCHEMA
  	],
  	bootstrap: [ MainComponent ]
})

export class AppModule {}
