export interface Resume {
	title: string;
	introduction: string;
	closure?: string;
	updated_at?: string;
}

export interface Contacts {
	email: string;
	phone: string;
	created_at?: string;
	deleted_at?: string;
	resume_id?: string;
}

export interface Education {
	schoolName: string;
	studyNameTime: {}[];
	briefFocus: string;
	created_at?: string;
	deleted_at?: string;
	resume_id?: string;
}

export interface TechBox {
	label: string;
	content: {}[];
	created_at?: string;
	deleted_at?: string;
	resume_id?: string;
}

export interface Experience {
	businessName: string;
	position: string;
	timeLength: string;
	briefDuties: string;
	created_at?: string;
	deleted_at?: string;
	resume_id?: string;
}

// export const RESUME_DATA: Resume = {
// 	title: 'Rob\'s Online Résumé',
// 	introduction: `Actively programming and drafting web apps daily for six months as a front-end developer with an enterprise solutions company has shown me a lot. That experience has taught me how a multi-part software product is put together and sold. This gave me a deeper respect for the development process and the combination of skills needed to create software. Participating in a scrum team to develop software for a micro-service infrastructure was a challenge that bettered my communication and task management skills. Developing web components from technical mocks to working web-apps for a large corporate user base allowed me to discover my passion for code. A self leader and self educator, I have been focused in TypeScript's strong typing patterns and (S)CSS's UI aesthetics. I have experience with Observables and Promise patterns, and I have been tinkering with sockets. I am looking for a place to learn and grow my programming skills and professional experiences.`,
// 	closure: `Thank you for taking the time to read my Super Résumé!! ;)`,
// 	contact: {
// 		email: 'rdev1163@outlook.com',
// 		phone: '503-298-6007'
// 	},
// 	techBox: [
// 		{
// 			label: 'Languages',
// 			content: 'Typescript, Javascript, CSS, SCSS, HTML, C++'
// 		},
// 		{
// 			label: 'Frameworks and Libraries',
// 			content: 'Angular 2, Cordova, Ionic, Express, JQuery'
// 		},
// 		{
// 			label: 'Platforms',
// 			content: 'Web-Browsers, Mobile, NodeJS'
// 		}
// 	],
// 	experience: [
// 		{
// 			businessName: `Mumba Cloud`,
// 			position: 'Jr. Developer',
// 			timeLength: 'Aug. 28, 2016 - Feb. 27, 2017',
// 			briefDuties: 'Responsible for translating visual mock-ups to Angular 2 modules and wiring up the frames to the backend\'s endpoints using WAMP Sockets and libraries. Drafting mock-ups from scratch and managing tasks using Target Process and collaborating using Skype in a scrum team.'
// 		}
// 	],
// 	education: [
// 		{
// 			schoolName: 'Self Studied JavaScript - Web Development',
// 			timeLength: ['6 months with TypeScript', '8 months of JavaScript'],
// 			briefFocus: `My focus has been Angular. I am self taught with months of enterprise development experience. I have no formal training, however I do posses a passion for code and I love to see how it all works and connects. I watch a lot of lectures and talks on Youtube, especially when I am uncertain of how to solve a problem. If I am unfamiliar with a syntax I dive into books and manuals. I also participate in a code mentorship program and take free online classes such as Harvard's CS50.net and EdX for understanding coding concepts.`
// 		}
// 	]
// };
