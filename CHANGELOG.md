# ChangeLog 
>uses sem-ver 

## unreleased

## 04.14.2017 v0.6.1
[code] added update at to `resume.component`
[refactor] rearranged some text in `resume.component`

## 04.14.2017 v0.6.1
[refactor] fixed some wording and type consistencies
[code] added a download link with Google Docs 

## 04.13.2017 v0.6.0
[code] hard coded Skype to `resume.conponent`'s contacts
[style] altered contact anchors size

## 04.13.2017 v0.5.0 
[refactor] moved content data for `resume.component` to `online-resume-backend`
[refactor] renamed `resume.data` to `resume.model` because it held interfaces

## 04.07.2017   v0.4.1
[bug] fixed toastr error / success popup 


## 04.07.2017   v0.4.0
[bug] fixed `message.component`'s socket connection
[code] added toastr to `socket.service` and `main.component`
[code] created `socket.service`

## 03.09.2017  v0.3.0

[code] Added `ng2-toastr` to `resume.component` and `about.component`

## 03.09.2017 v0.2.0
[refactor] html structure in `about.component`
[refactor] combined `resume.scss` and `about.scss` into `main.scss`
[style] styled `resume.component`
[code] created about page

## version 0.1.0

[init] base code modified from template seed
[code] created `resume.component`
[code] added static messenger to `resume.compoennt`
[code] created `main.component` with nav-tabs