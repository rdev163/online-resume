var express = require('express');
var app = express();

app.use(express.static(__dirname + '/js'));
app.set('view engine', 'jade');

app.get('/src', function(req, res) {
	console.log('hit');
	res.render('index', function(err, html) {
		if (err) {
			console.log(err);
		}
		else {
			res.send(html)
		}
	});
});